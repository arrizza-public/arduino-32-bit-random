import random
import statistics
import time

# creating a simple data - set
random.seed(time.time())
maxval = 10
sample = []
for i in range(1_000_000):
    sample.append(random.randint(1, maxval))

print(f'Std : {statistics.stdev(sample): >.3f} exp = {0.2873 * maxval: >.3f}')
print(f'mean: {statistics.mean(sample): >.3f} exp = {(maxval + 1) / 2.0: >.3f}')

counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for i in range(1_000_000):
    if sample[i] < 0 or sample[i] > 10:
        print(f'huh? [{i}]{sample[i]}')
        continue
    counts[sample[i]] += 1

print(counts)
