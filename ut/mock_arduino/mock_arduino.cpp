#include "mock_arduino.h"

#include <stdio.h>
#include <string.h>

#include "Arduino.h"

int TCCR2B = 0;
int OCR2A = 0;
int TCCR2A = 0;
int TIMSK2 = 0;

MockSerial Serial;
mock_values_t mock_values;

void mock_clear_digital_write()
{
    mock_values.digital_posn = 0;
    for (int i = 0; i < 2048; ++i) {
        mock_values.digital_write[i] = 0;
    }
}

void mock_init()
{
    mock_values.cbi_called_count = 0;
    mock_values.sbi_called_count = 0;
    mock_values.cli_called_count = 0;
    mock_values.sei_called_count = 0;
    mock_values.analog_read = 0;
    mock_clear_digital_write();
    mock_clear_serial_out();
}

void mock_clear_serial_out()
{
    memset(mock_values.serial_out, 0x00, sizeof(mock_values.serial_out));
}

MockSerial::MockSerial()
{
}

void MockSerial::begin(int num)
{
    (void)num;
}

char MockSerial::read()
{
    return 0x00;
}

void MockSerial::print(const char* s)
{
    strcat(mock_values.serial_out, s);
}

void MockSerial::print(const char c)
{
    (void)c;
    // TODO:  strcat(mock_values.serial_out, );
}

void MockSerial::print(unsigned int v, int format)
{
    char buf[30];
    if (format == DEC) {
        sprintf(buf, "%d", v);
    } else {
        sprintf(buf, "%X", v);
    }
    strcat(mock_values.serial_out, buf);
}

void MockSerial::println()
{
    strcat(mock_values.serial_out, "\x0A");
}

void MockSerial::println(const char* s)
{
    print(s);
    strcat(mock_values.serial_out, "\x0A");
}

void MockSerial::println(int v, int format)
{
    print(v, format);
    strcat(mock_values.serial_out, "\x0A");
}

int MockSerial::available()
{
    return 1;
}

void pinMode(int pin, int mode)
{
    (void)pin;
    (void)mode;
}

void cbi(int port, int bit)
{
    (void)port;
    (void)bit;
    mock_values.cbi_called_count++;
}

void sbi(int port, int bit)
{
    (void)port;
    (void)bit;
    mock_values.sbi_called_count++;
}

void sei(void)
{
    mock_values.sei_called_count++;
}

void cli(void)
{
    mock_values.cli_called_count++;
}

void delayMicroseconds(int delay)
{
    (void)delay;
}

int analogRead(uint8_t pin)
{
    return mock_values.analog_read;
}

void digitalWrite(uint8_t pin, uint8_t val)
{
    (void)pin;
    mock_values.digital_write[mock_values.digital_posn++] = val;
}

void delay(unsigned long ms)
{
    (void)ms;
}

void randomSeed(uint32_t seed)
{
    (void)seed;
};
