#ifndef _ARDUINO_H
#define _ARDUINO_H

#include <stdint.h>

#define __COMPILING_AVR_LIBC__
#define F_CPU 16000000L
#define ARDUINO 10005
#define ARDUINO_
#define ARDUINO_ARCH_AVR

#ifdef __AVR_ATmega2560__
    #undef __AVR_ATmega2560__
    #define __AVR_ATmega2560__
#endif

#ifdef __THROW
    #undef __THROW
    #define __THROW
#endif

#define DEC 10
#define HIGH 0x1
#define LOW 0x0

#define _BV(v) v

#define CS20 0b001
#define CS21 0b010
#define CS22 0b100
extern int TCCR2B;

extern int OCR2A;

#define WGM21 0b001
extern int TCCR2A;

#define OCIE2A 0b001;
extern int TIMSK2;

static const uint8_t A0 = 14;

#define OUTPUT 0
void pinMode(int pin, int mode);

int analogRead(uint8_t pin);
void digitalWrite(uint8_t pin, uint8_t val);
void delay(unsigned long ms);
void delayMicroseconds(int delay);
void sbi(int port, int bit);
void cbi(int port, int bit);
extern "C" {
long random(void);
}
void randomSeed(uint32_t seed);

class MockSerial {
public:
    MockSerial();
    void begin(int num);
    char read();
    void print(const char* s);
    void print(const char c);
    void print(unsigned int, int);
    void println();
    void println(const char* s);
    void println(int, int);
    int available();
};

#endif
