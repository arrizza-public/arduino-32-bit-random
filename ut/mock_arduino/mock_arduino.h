#ifndef _MOCK_H
#define _MOCK_H

#include <stdint.h>

typedef struct _mock_values {
    int cbi_called_count;
    int sbi_called_count;
    int sei_called_count;
    int cli_called_count;
    char serial_out[2048];
    int analog_read;
    uint8_t digital_write[2048];
    int digital_posn;
} mock_values_t;
extern mock_values_t mock_values;

void mock_init();
void mock_clear_serial_out();
void mock_clear_digital_write();
#endif
