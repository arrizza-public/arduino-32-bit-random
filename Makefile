.PHONY : all clean help  core core-init core-build core-link arduino-32-bit-random arduino-32-bit-random-init arduino-32-bit-random-build arduino-32-bit-random-link ut ut-init ut-build ut-link
#-- build all
all:  core core-init core-build core-link arduino-32-bit-random arduino-32-bit-random-init arduino-32-bit-random-build arduino-32-bit-random-link ut ut-init ut-build ut-link
#-- build core
core: core-init core-build core-link
#-- build arduino-32-bit-random
arduino-32-bit-random: arduino-32-bit-random-init arduino-32-bit-random-build arduino-32-bit-random-link
#-- build ut
ut: ut-init ut-build ut-link

# ==== core

#-- core: initialize for debug build
core-init:
	@mkdir -p debug
	@mkdir -p debug/core-dir

-include debug/core-dir/CDC.cpp.d
debug/core-dir/CDC.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/CDC.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/CDC.cpp -o debug/core-dir/CDC.cpp.o
-include debug/core-dir/HardwareSerial.cpp.d
debug/core-dir/HardwareSerial.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial.cpp -o debug/core-dir/HardwareSerial.cpp.o
-include debug/core-dir/HardwareSerial0.cpp.d
debug/core-dir/HardwareSerial0.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial0.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial0.cpp -o debug/core-dir/HardwareSerial0.cpp.o
-include debug/core-dir/HardwareSerial1.cpp.d
debug/core-dir/HardwareSerial1.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial1.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial1.cpp -o debug/core-dir/HardwareSerial1.cpp.o
-include debug/core-dir/HardwareSerial2.cpp.d
debug/core-dir/HardwareSerial2.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial2.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial2.cpp -o debug/core-dir/HardwareSerial2.cpp.o
-include debug/core-dir/HardwareSerial3.cpp.d
debug/core-dir/HardwareSerial3.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial3.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial3.cpp -o debug/core-dir/HardwareSerial3.cpp.o
-include debug/core-dir/IPAddress.cpp.d
debug/core-dir/IPAddress.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/IPAddress.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/IPAddress.cpp -o debug/core-dir/IPAddress.cpp.o
-include debug/core-dir/PluggableUSB.cpp.d
debug/core-dir/PluggableUSB.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/PluggableUSB.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/PluggableUSB.cpp -o debug/core-dir/PluggableUSB.cpp.o
-include debug/core-dir/Print.cpp.d
debug/core-dir/Print.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/Print.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/Print.cpp -o debug/core-dir/Print.cpp.o
-include debug/core-dir/Stream.cpp.d
debug/core-dir/Stream.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/Stream.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/Stream.cpp -o debug/core-dir/Stream.cpp.o
-include debug/core-dir/Tone.cpp.d
debug/core-dir/Tone.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/Tone.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/Tone.cpp -o debug/core-dir/Tone.cpp.o
-include debug/core-dir/USBCore.cpp.d
debug/core-dir/USBCore.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/USBCore.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/USBCore.cpp -o debug/core-dir/USBCore.cpp.o
-include debug/core-dir/WMath.cpp.d
debug/core-dir/WMath.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/WMath.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/WMath.cpp -o debug/core-dir/WMath.cpp.o
-include debug/core-dir/WString.cpp.d
debug/core-dir/WString.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/WString.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/WString.cpp -o debug/core-dir/WString.cpp.o
-include debug/core-dir/abi.cpp.d
debug/core-dir/abi.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/abi.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/abi.cpp -o debug/core-dir/abi.cpp.o
-include debug/core-dir/main.cpp.d
debug/core-dir/main.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/main.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/main.cpp -o debug/core-dir/main.cpp.o
-include debug/core-dir/new.cpp.d
debug/core-dir/new.cpp.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/new.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/new.cpp -o debug/core-dir/new.cpp.o

-include debug/core-dir/WInterrupts.c.d
debug/core-dir/WInterrupts.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/WInterrupts.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/WInterrupts.c -o debug/core-dir/WInterrupts.c.o
-include debug/core-dir/hooks.c.d
debug/core-dir/hooks.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/hooks.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/hooks.c -o debug/core-dir/hooks.c.o
-include debug/core-dir/wiring.c.d
debug/core-dir/wiring.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring.c -o debug/core-dir/wiring.c.o
-include debug/core-dir/wiring_analog.c.d
debug/core-dir/wiring_analog.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_analog.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_analog.c -o debug/core-dir/wiring_analog.c.o
-include debug/core-dir/wiring_digital.c.d
debug/core-dir/wiring_digital.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_digital.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_digital.c -o debug/core-dir/wiring_digital.c.o
-include debug/core-dir/wiring_pulse.c.d
debug/core-dir/wiring_pulse.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_pulse.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_pulse.c -o debug/core-dir/wiring_pulse.c.o
-include debug/core-dir/wiring_shift.c.d
debug/core-dir/wiring_shift.c.o: /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_shift.c
	avr-gcc -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  -g -Os -Wall  /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_shift.c -o debug/core-dir/wiring_shift.c.o

#-- core: compile arduino core source files
core-build:  /usr/share/arduino/hardware/arduino/avr/cores/arduino/CDC.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial0.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial1.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial2.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/HardwareSerial3.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/IPAddress.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/PluggableUSB.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/Print.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/Stream.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/Tone.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/USBCore.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/WInterrupts.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/WMath.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/WString.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/abi.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/hooks.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/main.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/new.cpp /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_analog.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_digital.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_pulse.c /usr/share/arduino/hardware/arduino/avr/cores/arduino/wiring_shift.c

#-- core: create arduino core library
core-link: debug/core.a

debug/core.a:  debug/core-dir/CDC.cpp.o debug/core-dir/HardwareSerial.cpp.o debug/core-dir/HardwareSerial0.cpp.o debug/core-dir/HardwareSerial1.cpp.o debug/core-dir/HardwareSerial2.cpp.o debug/core-dir/HardwareSerial3.cpp.o debug/core-dir/IPAddress.cpp.o debug/core-dir/PluggableUSB.cpp.o debug/core-dir/Print.cpp.o debug/core-dir/Stream.cpp.o debug/core-dir/Tone.cpp.o debug/core-dir/USBCore.cpp.o debug/core-dir/WMath.cpp.o debug/core-dir/WString.cpp.o debug/core-dir/abi.cpp.o debug/core-dir/main.cpp.o debug/core-dir/new.cpp.o debug/core-dir/WInterrupts.c.o debug/core-dir/hooks.c.o debug/core-dir/wiring.c.o debug/core-dir/wiring_analog.c.o debug/core-dir/wiring_digital.c.o debug/core-dir/wiring_pulse.c.o debug/core-dir/wiring_shift.c.o
	rm -f debug/core.a
	avr-ar rcs debug/core.a debug/core-dir/CDC.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/HardwareSerial.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/HardwareSerial0.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/HardwareSerial1.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/HardwareSerial2.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/HardwareSerial3.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/IPAddress.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/PluggableUSB.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/Print.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/Stream.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/Tone.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/USBCore.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/WInterrupts.c.o
	avr-ar rcs debug/core.a debug/core-dir/WMath.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/WString.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/abi.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/hooks.c.o
	avr-ar rcs debug/core.a debug/core-dir/main.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/new.cpp.o
	avr-ar rcs debug/core.a debug/core-dir/wiring.c.o
	avr-ar rcs debug/core.a debug/core-dir/wiring_analog.c.o
	avr-ar rcs debug/core.a debug/core-dir/wiring_digital.c.o
	avr-ar rcs debug/core.a debug/core-dir/wiring_pulse.c.o
	avr-ar rcs debug/core.a debug/core-dir/wiring_shift.c.o

#-- core: clean files in this target
core-clean:
	rm -f debug/core.a
	rm -f debug/core-dir/*.o
	rm -f debug/core-dir/*.d

# ==== arduino-32-bit-random

#-- arduino-32-bit-random: initialize for debug build
arduino-32-bit-random-init:
	@mkdir -p debug
	@mkdir -p debug/arduino-32-bit-random-dir/src

-include debug/arduino-32-bit-random-dir/src/main.cpp.d
debug/arduino-32-bit-random-dir/src/main.cpp.o: src/main.cpp
	avr-g++ -MMD -c -ffunction-sections -fdata-sections -mmcu=atmega328p -DF_CPU=16000000L -DUSB_VID=null -DUSB_PID=null -DARDUINO=106 -fno-exceptions -fno-threadsafe-statics -std=c++11 -g -Os -Wall  "-Isrc"  "-I/usr/share/arduino/hardware/arduino/avr/cores/arduino"  "-I/usr/share/arduino/hardware/arduino/avr/variants/standard"  src/main.cpp -o debug/arduino-32-bit-random-dir/src/main.cpp.o

#-- arduino-32-bit-random: build sketch
arduino-32-bit-random-build: src/main.cpp 

#-- arduino-32-bit-random: link sketch and core
arduino-32-bit-random-link: core debug/arduino-32-bit-random.hex

debug/arduino-32-bit-random.elf: debug/arduino-32-bit-random-dir/src/main.cpp.o  debug/core.a
	avr-gcc -Os -Wl,--gc-sections -mmcu=atmega328p -o debug/arduino-32-bit-random.elf debug/arduino-32-bit-random-dir/src/main.cpp.o  debug/core.a -lm

debug/arduino-32-bit-random.eep: debug/arduino-32-bit-random.elf
	avr-objcopy -O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0 debug/arduino-32-bit-random.elf debug/arduino-32-bit-random.eep

debug/arduino-32-bit-random.hex: debug/arduino-32-bit-random.elf debug/arduino-32-bit-random.eep
	avr-objcopy -O ihex -R .eeprom debug/arduino-32-bit-random.elf debug/arduino-32-bit-random.hex

#-- arduino-32-bit-random: upload to arduino
arduino-32-bit-random-upload: arduino-32-bit-random-link
	avrdude -v -C/usr/share/arduino/hardware/tools/avrdude.conf -patmega328p -carduino -P/dev/ttyUSB0 -b57600 -D -Uflash:w:debug/arduino-32-bit-random.hex:i -Ueeprom:w:debug/arduino-32-bit-random.eep:i

#-- arduino-32-bit-random: clean files in this target
arduino-32-bit-random-clean:
	rm -f debug/arduino-32-bit-random.elf
	rm -f debug/arduino-32-bit-random.eep
	rm -f debug/arduino-32-bit-random.hex
	rm -f debug/arduino-32-bit-random-dir/src/*.o
	rm -f debug/arduino-32-bit-random-dir/src/*.d

# ==== ut

#-- ut: initialize for debug build
ut-init:
	@mkdir -p debug
	@mkdir -p debug/ut-dir/ut/mock_arduino
	@mkdir -p debug/ut-dir/ut

-include debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.d
debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.o: ut/mock_arduino/mock_arduino.cpp
	g++ -MMD -c "-Isrc" "-Iut/mock_arduino"  -g -fdiagnostics-color=always -fprofile-arcs -ftest-coverage -DGTEST_HAS_PTHREAD=1 -std=gnu++20 -D_UCRT -D_GNU_SOURCE  ut/mock_arduino/mock_arduino.cpp -o debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.o
-include debug/ut-dir/ut/ut.cpp.d
debug/ut-dir/ut/ut.cpp.o: ut/ut.cpp
	g++ -MMD -c "-Isrc" "-Iut/mock_arduino"  -g -fdiagnostics-color=always -fprofile-arcs -ftest-coverage -DGTEST_HAS_PTHREAD=1 -std=gnu++20 -D_UCRT -D_GNU_SOURCE  ut/ut.cpp -o debug/ut-dir/ut/ut.cpp.o

#-- ut: build source files
ut-build: debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.o debug/ut-dir/ut/ut.cpp.o 

debug/ut: debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.o debug/ut-dir/ut/ut.cpp.o 
	g++ debug/ut-dir/ut/mock_arduino/mock_arduino.cpp.o debug/ut-dir/ut/ut.cpp.o    -lgtest -lpthread -lgcov -lgtest_main  -o debug/ut

#-- ut: link
ut-link: debug/ut ut-build

#-- ut: show coverage
ut-cov:
	gcovr --html-details --sort=uncovered-percent --print-summary -o debug/ut.html --filter src 
	@echo "see debug/ut.html" for HTML report

# used to pass args in cpp and gtest cmd lines
%:
	@:
#-- ut: run executable
ut-run: ut-link
	debug/ut $(filter-out $@,$(MAKECMDGOALS))

#-- ut: reset coverage info
ut-cov-reset:
	rm -f debug/ut-dir/ut/mock_arduino/*.gcda
	rm -f debug/ut-dir/ut/*.gcda

#-- ut: clean files in this target
ut-clean: ut-cov-reset
	rm -f debug/ut-dir/ut/mock_arduino/*.o
	rm -f debug/ut-dir/ut/mock_arduino/*.d
	rm -f debug/ut-dir/ut/mock_arduino/*.gcno
	rm -f debug/ut-dir/ut/*.o
	rm -f debug/ut-dir/ut/*.d
	rm -f debug/ut-dir/ut/*.gcno
	rm -f debug/ut
	rm -f debug/ut.html
	rm -f debug/ut.css
	rm -f debug/ut.**.html

#-- clean files
clean: core-clean arduino-32-bit-random-clean ut-clean 

help:
	@printf "Available targets:\n"
	@printf "  [32;01mall                                [0m build all\n"
	@printf "  [32;01marduino-32-bit-random              [0m build arduino-32-bit-random\n"
	@printf "    [32;01marduino-32-bit-random-build        [0m arduino-32-bit-random: build sketch\n"
	@printf "    [32;01marduino-32-bit-random-clean        [0m arduino-32-bit-random: clean files in this target\n"
	@printf "    [32;01marduino-32-bit-random-init         [0m arduino-32-bit-random: initialize for debug build\n"
	@printf "    [32;01marduino-32-bit-random-link         [0m arduino-32-bit-random: link sketch and core\n"
	@printf "    [32;01marduino-32-bit-random-upload       [0m arduino-32-bit-random: upload to arduino\n"
	@printf "  [32;01mclean                              [0m clean files\n"
	@printf "  [32;01mcore                               [0m build core\n"
	@printf "    [32;01mcore-build                         [0m core: compile arduino core source files\n"
	@printf "    [32;01mcore-clean                         [0m core: clean files in this target\n"
	@printf "    [32;01mcore-init                          [0m core: initialize for debug build\n"
	@printf "    [32;01mcore-link                          [0m core: create arduino core library\n"
	@printf "  [32;01mhelp                               [0m this help info\n"
	@printf "  [32;01mut                                 [0m build ut\n"
	@printf "    [32;01mut-build                           [0m ut: build source files\n"
	@printf "    [32;01mut-clean                           [0m ut: clean files in this target\n"
	@printf "    [32;01mut-cov                             [0m ut: show coverage\n"
	@printf "    [32;01mut-cov-reset                       [0m ut: reset coverage info\n"
	@printf "    [32;01mut-init                            [0m ut: initialize for debug build\n"
	@printf "    [32;01mut-link                            [0m ut: link\n"
	@printf "    [32;01mut-run                             [0m ut: run executable\n"
	@printf "\n"
