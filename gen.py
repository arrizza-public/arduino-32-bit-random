import sys

from pyalamake import alamake

sh1 = alamake.create_arduino_shared()
# where the Arduino IDE saves it's libraries
if alamake.is_macos:
    sh1.set_library_root('~/Library/Arduino15/libraries')
else:  # ubuntu
    sh1.set_library_root('~/Arduino/libraries')

sh1.set_boardid('nano-atmega328old')
port = alamake.get_port('1A86:7523')
if not port:
    sys.exit(1)
# otherwise, set port manually
# if alamake.is_macos:
#     port = '/dev/tty.usbserial-113240'
# elif alamake.is_win:
#     port = 'COM3'
# else:  # ubuntu
#     port = '/dev/ttyUSB0'
sh1.set_avrdude_port(port)
# at this point sh1 is only missing core related settings

# === arduino core
core = alamake.create('core', 'arduino-core', shared=sh1)
# no additional directories, libs, etc. needed
core.check()

# === arduino tgt
tgt = alamake.create('arduino-32-bit-random', 'arduino', shared=sh1)
tgt.add_sources([
    'src/main.cpp',
])
tgt.add_include_directories(['src'])

# === gtest
ut = alamake.create('ut', 'gtest')

# add homebrew dirs if running on macOS
ut.add_homebrew()

ut.add_include_directories([
    'src',
    'ut/mock_arduino',
])
ut.add_sources([
    'ut/mock_arduino/mock_arduino.cpp',
    'ut/ut.cpp',
])
ut.add_link_libraries('gtest_main')
ut.add_coverage(['src'])

# === generate makefile for all targets
alamake.makefile()
