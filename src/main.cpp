// --------------------
//! @file
//! Copyright(c) All Rights Reserved
//!
//! Summary: generate and check 32-bit random seed

// cppcheck-suppress missingInclude
#include "Arduino.h"
#include <math.h>

static void do_seed();
static uint32_t get_seed(uint8_t pin);
static void do_random();

//! calculate seeds only
static bool seeds_only = false;

//! holds info to calculate statistics on the fly (mean, stddev)
static struct
{
    // holds the number of elements so far
    uint32_t num_elems = 0;
    // holds the current sum of all elementss
    double sum = 0.0;
    //  holds the current mean
    double mean = NAN;
    // holds the current mean2
    double mean2 = 0.0;
    // holds the current variance
    double variance = NAN;
    // holds the current standard deviation
    double stddev = NAN;
} stat_info;
static void update_stats(double val);

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    Serial.begin(115200);
    Serial.println("Starting");
    do_seed();
}

// --------------------
//! Arduino main loop
//! calculate seeds and check if they are random
//! and repeat
// cppcheck-suppress unusedFunction
void loop()
{
    static uint32_t count = 0;

    // if this is the first time through, or the user has requested it, set the random seed
    if (Serial.available() > 0) {
        do_seed();
    }

    ++count;

    if (seeds_only) {
        // show a continual series of seeds
        char buf[100];

        uint32_t seed = get_seed(A0);
        sprintf(buf, "seed: 0x%08lX   %10lu", seed, seed);
        Serial.println(buf);
        delay(300);
    } else {
        // using the last seed, do an analysis of the random values
        do_random();
    }
}

// --------------------
//! generate a seed and use it to for srandom()
void do_seed()
{
    // clear out the incoming buffer
    uint32_t seed = 0;
    while (Serial.available() > 0) {
        int ch = Serial.read();
        if (isdigit(ch)) {
            seed = (seed * 10) + ch - '0';
        } else if (ch == 's') {
            seeds_only = !seeds_only;
        }
    }

    // the user may have entered a seed (decimal),
    // if not generate one
    if (seed == 0) {
        seed = get_seed(A0);
    }

    // set the random seed
    char buf[200];
    sprintf(buf, "seed: %08lX", seed);
    Serial.println(buf);
    srandom(seed);
}

// --------------------
//! calculate a 32 bit seed from 4 reads from analog pin
//! use a hash algorithm to mix/combine the reads into a "more random" value
//! see One-at-a-Time Hash  http://burtleburtle.net/bob/hash/doobs.html
//! @param pin the Analog pin to get a "random" value from
//! @return the seed
uint32_t get_seed(const uint8_t pin)
{
    static uint32_t last_seed = 0;

    uint32_t seed = 0;
    int count = 0;
    do {
        for (int i = 0; i < 4; ++i) {
            seed += analogRead(pin); // 10 bit resolution
            seed += (seed << 10U);
            seed ^= (seed >> 6U);
        }
        seed += (seed << 3U);
        seed ^= (seed >> 11U);
        seed += (seed << 15U);
        count++;
    } while ((seed == last_seed) && (count < 5));
    last_seed = seed;
    return seed;
}

// --------------------
//! update stats with the current seed value
//!
//! @param val  the new value in the sequence
void update_stats(double val)
{
    stat_info.num_elems += 1;

    double delta;
    if (isnan(stat_info.mean)) {
        delta = val;
        stat_info.mean = val;
    } else {
        delta = val - stat_info.mean;
        stat_info.mean += delta / (double)stat_info.num_elems;
    }

    double delta2 = val - stat_info.mean;
    stat_info.mean2 += delta * delta2;

    if (stat_info.num_elems < 2) {
        stat_info.variance = NAN;
        stat_info.stddev = NAN;
    } else {
        stat_info.variance = stat_info.mean2 / (double)(stat_info.num_elems - 1);
        stat_info.stddev = sqrt(stat_info.variance);
    }
}

// --------------------
//! use random() to check if the sequence is random
void do_random()
{
    static uint32_t counts[11] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint32_t val = random(10) + 1;
    update_stats((double)val);
    counts[val]++;

    if (stat_info.num_elems % 1000 == 0) {
        char buf[200];

        char buf_mean[10];
        dtostrf(stat_info.mean, 9, 3, buf_mean);
        char buf_stddev[10];
        dtostrf(stat_info.stddev, 9, 3, buf_stddev);

        sprintf(buf, "num:%5lu, mean:%s, stdev:%s, current val:%10lu",
            stat_info.num_elems, buf_mean, buf_stddev, val);
        Serial.println(buf);
        sprintf(buf,
            "[1]:%5ld  [2]:%5ld  [3]:%5ld  [4]:%5ld  [5]:%5ld  [6]:%5ld  [7]:%5ld  [8]:%5ld  [9]:%5ld [10]:%5ld ",
            counts[1], counts[2], counts[3], counts[4], counts[5],
            counts[6], counts[7], counts[8], counts[9], counts[10]);
        Serial.println(buf);
    }
}
